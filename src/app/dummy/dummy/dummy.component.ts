import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { DynamicFormComponent } from '../../common/dynamic-form/containers/dynamic-form/dynamic-form.component';
import { FieldConfig } from '../../common/dynamic-form/models/field-config.interface';

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.css']
})
export class DummyComponent implements OnInit {
  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  
  config: {content: FieldConfig[],footer: FieldConfig[]} = {
    content : [
    {
      type: 'input',
      label: 'Full name',
      name: 'name',
      placeholder: 'Enter your name',
      validation: [Validators.required]
    },
    {
      type: 'input',
      label: 'Qualification',
      name: 'qualification',
      placeholder: 'Enter your Qualifications',
      validation: [Validators.required, Validators.minLength(4)]
    },
    {
      type: 'select',
      label: 'Favourite Food',
      name: 'food',
      options: ['Pizza', 'Hot Dogs', 'Knakworstje', 'burgers'],
      placeholder: 'Select an option',
      validation: [Validators.required]
    },
    {
      type: 'select',
      label: 'Fav Bevrages',
      name: 'drinks',
      options: ['Coffee','Lassi','Chaas'],
      placeholder: 'Select an option',
      validation: [Validators.required]
    },
    {
      type: 'select',
      label: 'Favourite Hobby',
      name: 'hobby',
      options: ['Gaming','Gaming','Code','Code'],
      placeholder: 'Select an option',
      validation: [Validators.required]
    },
  ],
  footer: [
    {
      label: 'Submit',
      name: 'submit',
      type: 'button',
      color: 'primary'
    },
    {
      label: 'Danger',
      name: 'Danger',
      type: 'button', 
      click: this.click,
      color: 'warn'
    }
  ]
  };

  state:any= {
    title: "Dummy Component",
    name: "New Dummy"
  }

  constructor() { }

  ngOnInit(): void {
  }

  click(){
   // do something 
  }

  submit($event?){}
}
