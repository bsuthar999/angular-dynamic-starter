import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '../common/dynamic-form/dynamic-form.module';
import { MaterialModule } from '../shared-imports/material/material.module';
import { DummyComponent } from './dummy/dummy.component';


@NgModule({
    imports: [
        CommonModule,
        DynamicFormModule,
        MaterialModule,
        ReactiveFormsModule,
    ],
    declarations: [
      DummyComponent
    ],
    entryComponents: [
      DummyComponent
    ],
    exports:[]
  })
export class DummyModule {}
  