import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Field } from '../../models/field.interface';
import { FieldConfig } from '../../models/field-config.interface';

@Component({
  selector: 'form-select',
  styleUrls: ['form-select.component.scss'],
  template: `
    <mat-form-field  
      [formGroup]="group" 
      appearance="outline" >

    <mat-label>{{ config.label }}</mat-label>
    
    <mat-select
      [formControlName]="config.name"
      placeholder="config.placeHolder">

      <mat-option *ngFor="let option of config.options" [value]="option">
      {{ option }}
      </mat-option>
    
      </mat-select>
  
  </mat-form-field>
  `
})
export class FormSelectComponent implements Field {
  config: FieldConfig;
  group: FormGroup;
}
