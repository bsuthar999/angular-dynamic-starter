import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedUIModule } from './shared-ui/shared-ui.module';
import { AppService } from './app.service';
import { DynamicFormModule } from './common/dynamic-form/dynamic-form.module';
import { DummyModule } from './dummy/dummy.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DynamicFormModule,
    OAuthModule.forRoot(),
    SharedUIModule,
    DummyModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
